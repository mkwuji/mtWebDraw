﻿using System;

namespace mtWebDraw.Models
{
    /// <summary>
    /// 日志表：Web图Diagrams信息表操作日志
    /// </summary>
    public class tLogChartDiagram
    {
    
        /// <summary>
        /// 标识
        /// </summary>
        public virtual Int64 ID { get; set; }

        /// <summary>
        /// Web图Diagram标识
        /// </summary>
        public virtual Int64 ChartDiagramID { get; set; }
        
        /// <summary>
        /// 操作时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
        
        /// <summary>
        /// 操作人
        /// </summary>
        public virtual Guid UserID { get; set; }
        
        /// <summary>
        /// 备份内容
        /// </summary>
        public virtual String BakContent { get; set; }
        
        /// <summary>
        /// 更改内容
        /// </summary>
        public virtual String UpdContent { get; set; }
        
    }
}