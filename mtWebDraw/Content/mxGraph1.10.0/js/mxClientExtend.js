﻿
//#region downloadDiagram

//用于保存图形组件为本地html文件
//a：图形组件；c,d：图形组件边框需要去掉的宽度
function downloadDiagram(a, c, d, filename) {
    c = c != null ? c : 0;
    d = d != null ? d : 0;

    var e = a.getGraphBounds();
    c = -e.x + c;
    d = -e.y + d;

    //得到图形的body内容
    var htmlStr = '';
    if (mxClient.IS_IE) {
        htmlStr = a.container.innerHTML;

        htmlStr = '<body><div style="position:absolute;left:' + c + 'px;top:' + d + 'px;">' + htmlStr + '</div></body>';
    } else {
        var b = document.createElement('body');

        for (a = a.container.firstChild; a != null ;) {
            g = a.cloneNode(true);
            b.appendChild(g);
            a = a.nextSibling
        }
        a = b.getElementsByTagName("g")[0];
        if (a != null ) {
            a.setAttribute("transform", "translate(" + c + "," + d + ")");
        }

        htmlStr = '<body style="overflow:auto;">' + b.innerHTML + '</body>';
        b = null;
    }

    //创建下载文件名及html内容
    var cname = (ChartInfo.cname ? ChartInfo.cname + "--" : "") + filename;
    var hrefUrl = "http://" + window.location.host + "/";

    if (htmlStr.indexOf('../../') == -1)
        htmlStr = htmlStr.replace(/\/Content\//g, hrefUrl + 'Content/'); //登陆版替换/Content/
    else
    {
        htmlStr = htmlStr.replace(/\.\.\/\.\.\/\.\.\/\.\.\/Content\//g, hrefUrl + 'Content/'); //未登陆版替换../../../../Content/ //Group图标之用
        htmlStr = htmlStr.replace(/\.\.\/\.\.\//g, hrefUrl + 'Content/Canvas/'); //未登陆版替换../../
    }

    htmlStr = '<!DOCTYPE html><html><head><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><meta http-equiv="Content-type" content="text/html; charset=gb2312" /><title>' + cname + '</title><link type="text/css" rel="stylesheet" href="' + hrefUrl + 'Content/Canvas/archive/grapheditor/css/grapheditor.css" /></head>' + htmlStr + '</html>';
    //实际证实不需要引用js便可显示正常
    //<script type="text/javascript">mxBasePath = "' + hrefUrl + 'Content/mxGraph1.10.0/";</script><script type="text/javascript" src="' + hrefUrl + 'Content/mxGraph1.10.0/js/mxClient.js"></script>

    //创建表单Post提交下载
    if (Ext.fly('downForm')) {
        document.body.removeChild(document.getElementById('downForm'));
    }

    var downForm = CreateDownForm('downForm', '/Tools/SavaDesktop');
    downForm.appendChild(CreateInputHidden('cname', cname));
    downForm.appendChild(CreateInputHidden('htmlOutHtml', htmlStr));
    document.body.appendChild(downForm);

    Ext.fly('downForm').dom.submit();
    document.body.removeChild(downForm);
}

//创建当前窗口Post下载Form
function CreateDownForm(id, action) {
    var downForm = document.createElement('form');
    downForm.id = id;
    downForm.style.display = 'none';
    downForm.action = action;
    downForm.method = 'post';
    return downForm;
}
//创建input，type为Hidden
function CreateInputHidden(name, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    return input;
}

//#endregion

//#region downCodeDiagram

//用于保存图形组件代码为本地txt文件
//graph：图形组件；filename：文件名
function downCodeDiagram(graph, filename)
{
    var enc = new mxCodec(mxUtils.createXmlDocument());
    var node = enc.encode(graph.getModel());
    var xml = mxUtils.getXml(node);

    xml = xml.replace(/\.\.\/\.\.\/\.\.\/\.\.\/Content\//g, '/Content/'); //未登陆版替换../../../../Content/ //Group图标之用
    xml = xml.replace(/\.\.\/\.\.\//g, '/Content/Canvas/'); //未登陆版替换../../

    //对xml没有<mxGraphModel></mxGraphModel>标记而从<root>开始的增加<mxGraphModel></mxGraphModel>
    if (xml && xml.substring(0, 6) == "<root>")
        xml = "<mxGraphModel>" + xml + "</mxGraphModel>";

    //创建表单Post提交下载
    if (Ext.fly('downForm')) {
        document.body.removeChild(document.getElementById('downForm'));
    }

    var cname = (ChartInfo.cname ? ChartInfo.cname + "--" : "") + filename;

    var downForm = CreateDownForm('downForm', '/Tools/SavaDesktop');
    downForm.appendChild(CreateInputHidden('cname', cname));
    downForm.appendChild(CreateInputHidden('htmlOutHtml', xml));
    downForm.appendChild(CreateInputHidden('type', '1'));
    document.body.appendChild(downForm);

    Ext.fly('downForm').dom.submit();
    document.body.removeChild(downForm);
}

//#endregion

//#region 判断绘图组件是否支持当前的浏览器：isBrowserSupported

//ExtJS加载完成后判断当前浏览器是否支持
//Ext.onReady(function () {
//    //检查浏览器是否支持
//    if (!mxClient.isBrowserSupported()) {
//        //如果浏览器不支持，则弹出错误信息
//        Ext.MessageBox.confirm("温馨提示", "在线绘图组件不支持您的浏览器，点击确定下载火狐浏览器！", function (_btn) {
//            if (_btn == "yes") {
//                window.location = "http://download.firefox.com.cn/releases/stub/official/zh-CN/Firefox-latest.exe";
//            } else {
//                window.close();
//            }
//        });
//    }
//});

//#endregion
