﻿/// <reference path="../ExtJS/adapter/ext/ext-base.js" />
/// <reference path="../ExtJS/ext-all.js" />
/// <reference path="../ExtJS/ExtJSextend.js" />
/// <reference path="tabCommon.js" />

var grid = null;
var dataWin = null;

//数据容器
var gridStore = new Ext.data.JsonStore({
    id: 'gridStore',
    fields: ['ID', 'ChartDiagramID', 'CreateTime', 'UserID', 'BakContent', 'UpdContent']
});

//加载grid的数据
function LoadData() {
    tabAjaxPost('/Ajax/DiagramLogList?id=' + DiagramInfo.id + "&cid=" + DiagramInfo.cid, function (json) {
        if (json.code == 1) {
            gridStore.loadData(json.data);
        } else {
            errorMessage(json.msg);
        }
    });
}

Ext.onReady(function () {

    //grid 行展开显示内容
    var gridExpander_tpl = new Ext.Template(
            '<p><b>时间：</b>　{CreateTime}　&nbsp;　<b>用户：</b>　{UserID:this.userFormat()}</p>',
            '<p><b>备份内容：</b></p>',
            '<p class="textselect">{BakContent}</p>',
            '<p><b>更改内容：</b></p>',
            '<p class="textselect">{UpdContent}</p>'
            );
    var gridExpander = new Ext.ux.grid.RowExpander({
        tpl: gridExpander_tpl
    });
    gridExpander_tpl.userFormat = function (data, o) {
        return GetUserDiaplayName(data);
    }

    //显示数据表格
    grid = new Ext.grid.EditorGridPanel({
        id: 'grid',
        scriptRow: true,
        store: gridStore,
        viewConfig: {
            firceFit: true,
            deferEmptyText: false,
            emptyText: '暂无流程图数据！',
            getRowClass: function (record, index) {
                if (!(index % 2)) {
                    return 'x-grid3-row';
                } else {
                    return 'x-grid3-row-alt';
                }
            }
        },
        columns: [
            new Ext.grid.RowNumberer(),
            gridExpander,
            { header: '标识', width: 60, dataIndex: 'ID', sortable: true, align: 'center', hidden: true },
            { header: 'Diagram标识', width: 70, dataIndex: 'ChartDiagramID', sortable: true, align: 'center', hidden: true },
            { header: '创建时间', width: 180, dataIndex: 'CreateTime', sortable: true, align: 'center' },
            { header: '操作人', width: 100, dataIndex: 'UserID', sortable: true, align: 'center', renderer: GetUserDiaplayName },
            { header: '备份内容', width: 300, dataIndex: 'BakContent', sortable: true, align: 'center' },
            { header: '更改内容', width: 300, dataIndex: 'UpdContent', sortable: true, align: 'center' }
        ],
        plugins: gridExpander,
        sm: new Ext.grid.RowSelectionModel({ singleSelect: true })
    });

    var _center = new Ext.Panel({
        region: 'center',
        layout: 'fit',
        items: [grid]
    });
    var _view = new Ext.Viewport({
        layout: 'border',
        items: [_center]
    });

    //加载部门与用户
    _userLoadData();

    //加载数据
    LoadData();

});
