﻿//tabPanel内嵌页请求父框架页统一方法，父框架页对应方法位于Index.js中

//获取用户信息
function GetLoginUser() {
    return parent.user;
}

//请求数据连接
function Ajax_lianjie() {
    parent.Ajax_lianjie();
}

//请求数据完成
function Ajax_wancheng() {
    parent.Ajax_wancheng();
}

//请求数据出错
function Ajax_chucuo() {
    parent.Ajax_chucuo();
}

//设置底部工具栏状态消息
function SetbbarStatus(message) {
    parent.SetbbarStatus(message);
}

//打开绘图选项卡
function OpenTabCanvas(id, title) {
    parent.CreateTabShow("Canvas_" + id, title + " - 绘图", 'icons-chartdiagram', '/Home?v=Canvas&cid=' + id + '&cname=' + title);
}

//打开流程图授权用户使用选项卡
function OpenTabAuthorizationUser(id, title) {
    parent.CreateTabShow("AuthorizationUser_" + id, title + " - 授权他人使用", 'icons-authorization', '/Home?v=AuthorizationUser&cid=' + id + '&cname=' + title);
}

//打开流程图图形历史日志选项卡
function OpenTabDiagramHistory(id, title, cid, ctitle) {
    parent.CreateTabShow("DiagramLog_" + id, ctitle + " - " + title + " - 修改历史", 'icons-base-preview', '/Home?v=DiagramLog&did=' + id + '&dname=' + title + '&cid=' + cid + '&cname=' + ctitle);
}

//Ext.Ajax.request以同步方式进行post请求数据：url-请求地址,successFunc-成功后回调方法
//示例：tabAjaxPost('/Ajax/LoginUserInfo', function (json) {
//    if (json.code == 1) { user = json.data; } else { errorMessage(json.msg); }
//});
function tabAjaxPost(url, successFunc) {
    tabAjaxPostData(url, '', successFunc);
}

//Ext.Ajax.request以同步方式进行post请求数据：url-请求地址,successFunc-成功后回调方法
//示例：tabAjaxPostData('/Ajax/LoginUserInfo', { ID: json[0].ID }, function (json) {
//    if (json.code == 1) { user = json.data; } else { errorMessage(json.msg); }
//});
function tabAjaxPostData(url, params, successFunc) {
    Ajax_lianjie();

    Ext.Ajax.request({
        url: url,
        method: 'post',
        async: false, //是否异步(true异步, false同步)
        params: params,
        success: function (response, opts) {
            var json = eval("(" + response.responseText + ")");
            successFunc(json);

            Ajax_wancheng();
        },
        failure: function (response, opts) {
            var error = "";
            if (action.failureType == 'connect') {
                error = '网络超时，请稍候再试！';
            } else {
                error = "未知网络错误！";
            }
            errorMessage(error);
            Ajax_chucuo();
        }
    });
}

//表单提交通用方法：varForm-是Ext.FormPanel的变量名,url-表单提交地址,successFunc-成功后回调方法
//示例：tabFormSubmit(loginForm, '/Ajax/UserLogin', function (json) {
//    if (json.code == 1) { user = json.data; } else errorMessage(json.msg);
//});
function tabFormSubmit(varForm, url, successFunc) {
    if (varForm.form.isValid()) {
        Ajax_lianjie();

        varForm.form.doAction('submit', {
            url: url,
            method: 'post',
            params: '',
            clientValidation: true,
            submitEmptyText: false,
            waitMsg: '正在提交中......',
            waitTitle: '提示',
            success: function (form, action) {
                successFunc(action.result);

                Ajax_wancheng();
            },
            failure: function (form, action) {
                var error = "";
                if (action.failureType == 'connect') {
                    error = '网络超时，请稍候再试！';
                } else {
                    error = "未知网络错误！";
                }
                errorMessage(error);

                Ajax_chucuo();
            }
        });
    }
}



//部门下拉列表数据容器
var _depStore = new Ext.data.JsonStore({
    id: '_depStore',
    fields: ['ID', 'FullName']
});
//部门下拉列表数据加载
function _depLoadData() {
    tabAjaxPost('/Ajax/SelectDepartmentList', function (json) {
        if (json.code == 1) {
            _depStore.loadData(json.data);
        } else {
            errorMessage(json.msg);
        }
    });
}
//根据ID获取部门名称
function GetDepartmentName(value)
{
    var depCount = _depStore.getCount();
    for (var i = 0; i < depCount; i++) {
        if (_depStore.getAt(i).json.ID.toString().toLowerCase() == value.toString().toLowerCase())
            return _depStore.getAt(i).json.FullName;
    }
    return "未知";
}


//用户下拉列表数据容器
var _userStore = new Ext.data.JsonStore({
    id: '_userStore',
    fields: ['ID', 'UserName', 'DepartmentID', 'FullName', 'DisplayName']
});
//用户下拉列表数据加载
function _userLoadData() {
    tabAjaxPost('/Ajax/SelectUserList', function (json) {
        if (json.code == 1) {
            _userStore.loadData(json.data);
        } else {
            errorMessage(json.msg);
        }
    });
}
//根据ID获取用户显示名称
function GetUserDiaplayName(value)
{
    var userCount = _userStore.getCount();
    for (var i = 0; i < userCount; i++) {
        if (_userStore.getAt(i).json.ID.toString().toLowerCase() == value.toString().toLowerCase())
            return _userStore.getAt(i).json.DisplayName;
    }
    return "未知";
}


//授权码数据容器
var codeStore = new Ext.data.JsonStore({
    id: 'codeStore',
    fields: ['ID', 'Name']
});
//固定的授权码数据
codeStore.loadData([{ ID: '0', Name: '无权限' }, { ID: '11', Name: '仅查看' }, { ID: '21', Name: '可编辑' }, { ID: '41', Name: '可删除' }]);

