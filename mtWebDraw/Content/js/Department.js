﻿/// <reference path="../ExtJS/adapter/ext/ext-base.js" />
/// <reference path="../ExtJS/ext-all.js" />
/// <reference path="../ExtJS/ExtJSextend.js" />
/// <reference path="tabCommon.js" />

var grid = null;
var dataForm = null;
var dataWin = null;

//数据容器
var gridStore = new Ext.data.JsonStore({
    id: 'gridStore',
    fields: ['ID', 'FullName', 'Caption']
});

//加载grid的数据
function LoadData() {
    tabAjaxPost('/Ajax/DepartmentList', function (json) {
        if (json.code == 1) {
            gridStore.loadData(json.data);
        } else {
            errorMessage(json.msg);
        }
    });
}

Ext.onReady(function () {

    //grid 行展开显示内容
    var gridExpander = new Ext.ux.grid.RowExpander({
        tpl: new Ext.Template(
            '<p>' + GetRowExpanderNbsp() + '<b>名称：</b>　{FullName}</p>',
            '<p>' + GetRowExpanderNbsp() + '<b>介绍：</b>　{Caption}</p>'
            )
    });

    //显示数据表格
    grid = new Ext.grid.GridPanel({
        id: 'grid',
        scriptRow: true,
        store: gridStore,
        viewConfig: {
            firceFit: true,
            deferEmptyText: false,
            emptyText: '暂无部门数据！',
            getRowClass: function (record, index) {
                if (!(index % 2)) {
                    return 'x-grid3-row';
                } else {
                    return 'x-grid3-row-alt';
                }
            }
        },
        columns: [
            new Ext.grid.RowNumberer(),
            gridExpander,
            { header: '标识', width: 260, dataIndex: 'ID', sortable: true, align: 'center', hidden: true },
            { header: '名称', width: 180, dataIndex: 'FullName', sortable: true, align: 'center' },
            { header: '介绍', width: 400, dataIndex: 'Caption', sortable: true, align: 'center' }
        ],
        plugins: gridExpander,
        sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),
        listeners: {
            'rowdblclick': function (grid, rowIndex, e) {
                EditData(rowIndex);
            },
            'rowcontextmenu': function (grid, rowIndex, e) {
                e.preventDefault();
                var grid_menus = new Ext.menu.Menu({
                    shadow: 'sides',
                    items: [{
                        text: '编辑',
                        iconCls: 'icons-department-edit',
                        handler: function () {
                            EditData(rowIndex);
                        }
                    }, {
                        text: '删除',
                        iconCls: 'icons-department-delete',
                        handler: function () {
                            DeleteData(rowIndex);
                        }
                    }]
                });
                grid_menus.showAt(e.getPoint());
            }
        }
    });

    //视图内容与布局
    var _center = new Ext.Panel({
        region: 'center',
        layout: 'fit',
        items: [grid],
        tbar: [{
            text: '新增',
            iconCls: 'icons-department-add',
            handler: function () {
                AddData();
            }
        }, {
            text: '编辑',
            iconCls: 'icons-department-edit',
            handler: function () {
                EditData();
            }
        }, {
            text: '删除',
            iconCls: 'icons-department-delete',
            handler: function () {
                DeleteData();
            }
        }, {
            text: '刷新',
            iconCls: 'icons-department-search',
            handler: function () {
                LoadData();
            }
        }]
    });
    var _view = new Ext.Viewport({
        layout: 'border',
        items: [_center]
    });

    //加载数据
    LoadData();

    //创建表单
    CreateForm();

});

//新增数据
function AddData()
{
    dataForm.getForm().reset();
    dataWin.setTitle("新建部门信息");
    dataWin.show();
}

//编辑行数据
function EditData(rowIndex)
{
    var row = GetSelectionRow('grid', rowIndex);
    if (row)
    {
        dataWin.setTitle("编辑部门信息 - " + row.json.FullName);
        dataWin.show();
        dataForm.getForm().loadRecord(row);
    }
    else
    {
        errorMessage("请先点选一行数据！");
    }
}

//删除行数据
function DeleteData(rowIndex)
{
    var row = GetSelectionRow('grid', rowIndex);
    if (row) {
        Ext.Msg.confirm('温馨提示', '您确定要删除该信息吗？', function (btn) {
            if (btn == 'yes') {
                tabAjaxPostData('/Ajax/DepartmentDelete', { ID: row.json.ID }, function (json) {
                    if (json.code == 1) {
                        LoadData();
                    } else {
                        errorMessage(json.msg);
                    }
                });
            }
        });
    }
    else {
        errorMessage("请先点选一行数据！");
    }
}

//创建表单
function CreateForm()
{
    //使用表单提示
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    dataForm = new Ext.FormPanel({
        labelWidth: 60,
        labelAlign: 'right',
        border: false,
        defaultType: 'textfield',
        buttomAlign: 'center',
        bodyStyle: 'padding: 15px',
        items: [{
            inputType: 'hidden',
            name: 'ID'
        }, {
            fieldLabel: '名称',
            name: 'FullName',
            anchor: '90%',
            allowBlank: false,
            emptyText: '请填写部门名称',
            blankText: '部门名称不能为空'
        }, {
            xtype: 'textarea',
            fieldLabel: '介绍',
            name: 'Caption',
            anchor: '90%',
            emptyText: '可选填介绍内容',
            maxLength: 400
        }],
        buttons: [{
            text: '提交',
            iconCls: 'icons-base-accept',
            handler: function () {
                SaveData();
            }
        }, {
            text: '退出',
            iconCls: 'icons-base-cancel',
            handler: function () {
                dataWin.hide();
            }
        }]
    });

    //表单显示的窗体
    dataWin = new Ext.Window({
        title: '部门信息',
        layout: 'fit',
        width: 400,
        height: 200,
        closeAction: 'hide',
        iconCls: 'icons-department',
        plain: true,
        modal: true,
        maskDisabled: true,
        items: dataForm
    });

}

//提交编辑
function SaveData()
{
    tabFormSubmit(dataForm, '/Ajax/DepartmentEdit', function (json) {
        if (json.code == 1) {
            alertMessage("操作成功！");
            dataWin.hide();
            LoadData();
        }
        else
            errorMessage(json.msg);
    });
}
